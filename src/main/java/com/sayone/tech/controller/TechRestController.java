package com.sayone.tech.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sayone.tech.entity.Job;
import com.sayone.tech.service.TechService;

@RestController
public class TechRestController {

	@Autowired
	private TechService techService;

	@GetMapping("joblist")
	public Collection<Job> jobList() {

		return techService.jobList();
	}

	@PostMapping("saveJob")
	public ModelAndView saveJob(HttpServletRequest request) {
		Date sdate=new Date();
		Date edate=new Date();
		try {
			sdate=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(request.getParameter("startDate"));
			edate=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(request.getParameter("endDate"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		System.out.println();
		Job job = new Job();
		job.setJobId(Integer.parseInt(request.getParameter("jobId")));
		job.setDescription(request.getParameter("description"));
		job.setStartDate(sdate);
		job.setEndDate(edate);
		job.setUpdatedDate(new Date());
		job.setErrors(request.getParameter("errors"));
		if(request.getParameter("activeFlag")==null) {
			job.setActiveFlag(false);
		}else {
			job.setActiveFlag(true);
		}
		
		
		int id=techService.saveJob(job);
		
		ModelAndView mav = new ModelAndView("redirect:/sayone");
	    return mav;
		
//		 return "redirect:/sayone";
	}
	
	@GetMapping("job")
	public Job getJob(int id) {

		return techService.getJob(id);
	}
	
	@GetMapping("deleteJob")
	public void deleteJob(int id) {

		techService.deleteJob(id);
	}
	
	@GetMapping("updateStatus")
	public int updateStatus(int id, int status) {

		return techService.updateStatus(id, status);
	}

}
