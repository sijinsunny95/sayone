package com.sayone.tech.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sayone.tech.entity.Job;

@Repository
public interface TechRepository extends JpaRepository<Job, Integer>{
	
	Job findByJobId(int id);
	
	List<Job> findAll();

	@Transactional
	@Modifying
	@Query("delete from Job where jobId=:id")
	void deleteJobId(int id);

	@Transactional
	@Modifying
	@Query("update Job set status=:status,updatedDate=:date where jobId=:id")
	void updateStatus(int id, int status,Date date);

}
