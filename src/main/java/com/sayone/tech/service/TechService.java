package com.sayone.tech.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sayone.tech.entity.Job;
import com.sayone.tech.repository.TechRepository;

@Service
public class TechService {

	@Autowired
	private TechRepository techRepository;

	public Collection<Job> jobList() {

		return techRepository.findAll();
	}

	public int saveJob(Job job) {
		// TODO Auto-generated method stub

		Job entity = techRepository.findByJobId(job.getJobId());
		if (entity == null) {
			entity = new Job();
			entity.setDescription(job.getDescription());
			entity.setStartDate(job.getStartDate());
			entity.setEndDate(job.getEndDate());
			entity.setUpdatedDate(new Date());
			entity.setStatus(0);
			entity.setErrors(job.getErrors());
			entity.setActiveFlag(job.isActiveFlag());
			return techRepository.save(entity).getJobId();
		} else {
			return techRepository.save(job).getJobId();
		}

	}

	public Job getJob(int id) {
		// TODO Auto-generated method stub
		return techRepository.findByJobId(id);
	}

	public void deleteJob(int id) {
		// TODO Auto-generated method stub
		
		techRepository.deleteJobId(id);
	}

	public int updateStatus(int id, int status) {
		// TODO Auto-generated method stub
		
		techRepository.updateStatus(id, status,new Date());
		
		return id;
	}

}
