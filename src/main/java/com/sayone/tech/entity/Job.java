package com.sayone.tech.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sayone.tech.util.JsonDateTimeDeSerializer;
import com.sayone.tech.util.JsonDateTimeSerializer;

@Entity
public class Job {

	@Id
	@GeneratedValue
	private int jobId;

	private String description;

	private int status;			// (run, rerun, kill, running)
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeSerializer.class)
	private Date startDate; 	// (Datetime)

	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeSerializer.class)
	private Date endDate; 		// (Datetime)

	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeSerializer.class)
	private Date updatedDate;	// (Datetime)

	private String errors;

	private boolean isActiveFlag;

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public boolean isActiveFlag() {
		return isActiveFlag;
	}

	public void setActiveFlag(boolean isActiveFlag) {
		this.isActiveFlag = isActiveFlag;
	}

}
