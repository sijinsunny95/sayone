<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>sayone</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css"
	href="resources/bootstrap5/css/bootstrap-datetimepicker.css">

<script type="text/javascript" charset="utf8"
	src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="resources/bootstrap5/js/bootstrap-datetimepicker.js"></script>
<style>
.btn {
	border: 1px solid;
}

.form-group input {
	border: 0px;
	border-top-style: none;
}

.form-group {
	margin-bottom: 10px;
}
</style>
</head>
<body style="padding: 30px;">

	<div>
		<div style="margin-bottom:32px">
			<h3>Job list</h3>

			<div style="float: right;margin-top: -25px;">
				<button class="btn btn-sm create">Create</button>

			</div>
		</div>
		
		<div style="background-color: #f2eaea;padding: 32px;">
			<table class="table">
				<thead>
					<th>Job Id</th>
					<th>Description</th>
					<th>Status</th>
					<th>Start date</th>
					<th>End date</th>
					<th>Updated date</th>
					<th>Errors</th>
					<th>IsActiveFlag</th>
					<th>Actions</th>

				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>


	<div class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<h5 class="modal-title">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="saveJob" method="post">
					<div class="modal-body">
						<div class="row">
							<input type="hidden" id="hiddenId" name="jobId" value="0" />
							<div class="col-md-6 form-group">
								<label>Description</label>
								<div class="form-control">
									<input type="text" class="description" name="description" />
								</div>
							</div>
							<!-- <div class="col-md-6 form-group">
								<label>Status</label>
								<div class="form-control">
									<input type="text" class="status" />
								</div>
							</div> -->
							<div class="col-md-6 form-group">
								<label> Start Date </label>
								<div class="form-control">
									<input type="text" class="datepicker startDate" name="startDate" />
								</div>
							</div>
							<div class="col-md-6 form-group">
								<label> End Date </label>
								<div class="form-control">
									<input type="text" class="datepicker endDate" name="endDate"/>
								</div>
							</div>

							<div class="col-md-6 form-group">
								<label> Errors </label>
								<div class="form-control">
									<input type="text" class="errors" name="errors"/>
								</div>
							</div>
							<div class="col-md-6 form-group">
								<label> Active Flag </label>
								<div class="">
									<input type="checkbox" checked="checked" class="activeFlag" name="activeFlag"/>
								</div>
							</div>

<!-- 							<input type="hidden" name="" value="true" /> -->
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-secondary close"
							data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="resources/tech.js"></script>

</body>
</html>