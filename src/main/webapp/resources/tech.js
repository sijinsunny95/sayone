$(document).ready(function() {

	$(".datepicker").datetimepicker();

	var dtable = $('.table').DataTable();

	retrieveJobDetails();

	function retrieveJobDetails() {

		$.ajax({
			url: "joblist",
			success: function(result) {

				$.each(result, function(inv, val) {


					var stat = "";
					console.log(val.status);
					if (val.status == 1) {
						stat = `<span class='green'>Running</span><br>
							<img src="resources/img/play.png" hidden class="stat start" stat="1" elm_id=`+ val.jobId + ` style="width:24px"/>
							<img src="resources/img/restart.png" class="stat restart" stat="1" elm_id=`+ val.jobId + ` style="width:28px"/>
							<img src="resources/img/stop.png" class="stat stop" stat="0" elm_id=`+ val.jobId + ` style="width:24px"/>`;
					} else {
						stat = `<span class='green'>Stoped</span><br>
							<img src="resources/img/play.png" class="stat start" stat="1" elm_id=`+ val.jobId + ` style="width:24px"/>
							<img src="resources/img/restart.png" hidden class="stat restart" stat="1" elm_id=`+ val.jobId + ` style="width:28px"/>
							<img src="resources/img/stop.png" hidden class="stat stop" stat="0" elm_id=`+ val.jobId + ` style="width:24px"/>`;
					}


					dtable
						.row.add([
							(inv + 1),
							val.description,
							stat,
							val.startDate,
							val.endDate,
							val.updatedDate,
							val.errors,
							val.activeFlag,
							'<button class="edit btn btn-sm btn-warning" elm_id="' + val.jobId + '">Edit</button>' +
							'<button class="delete btn btn-sm btn-danger" elm_id="' + val.jobId + '">Delete</button>'
						])
						.draw()
						.node();

				});
			}

		});
	}

	$(".create").on("click", function() {
		$(".modal").modal("show");
		$("#hiddenId").val(0);
		$(".description,.startDate,.endDate,.errors").val("");
	});
	$(".close").on("click", function() {
		$(".modal").modal("hide");
	});
	$("body").on("click", ".edit", function() {
		var jobId = $(this).attr("elm_id");

		$.ajax({
			url: "job",
			data: { id: jobId },
			success: function(val) {
				console.log(val);
				$(".modal").modal("show");
				$("#hiddenId").val(val.jobId);
				$(".description").val(val.description);
				$(".startDate").val(val.startDate);
				$(".endDate").val(val.endDate);
				$(".updatedDate").val(val.updatedDate);
				$(".errors").val(val.errors);

			}
		});
	});

	$("body").on("click", ".delete", function() {
		var jobId = $(this).attr("elm_id");
		var th = $(this).closest('tr');

		$.ajax({
			url: "deleteJob",
			data: { id: jobId },
			success: function(val) {
				th.remove();
			}
		});
	});

	$("body").on("click", ".stat", function() {
		var stat = $(this).attr("stat");
		var jobId = $(this).attr("elm_id");
		var th = $(this).closest("td");

		$.ajax({
			url: "updateStatus",
			data: {
				id: jobId,
				status: stat,
			},
			success: function(result) {

				if (stat == 0) {
					th.find(".start").removeAttr("hidden");
					th.find(".restart,.stop").attr("hidden", true);
				} else {
					th.find(".start").attr("hidden", true);
					th.find(".restart,.stop").removeAttr("hidden");
				}
			}
		});
	});

});